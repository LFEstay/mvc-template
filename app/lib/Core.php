<?php

    //mapping url : controller => method => parameter

    class Core {

        protected $currentController = 'maincontroller';
        protected $currentMethod = 'index';
        protected $parameters = [];

        public function __construct() {

            $url = $this->getUrl();
            $this->initController($url);
            $this->verifyMethod($url);

            $this->parameters = $url ? array_values($url) : [];
            call_user_func_array([$this->currentController, $this->currentMethod], $this->parameters);
        }

        public function getUrl() {

            if (isset($_GET['url'])) {

                $url = rtrim($_GET['url'], '/');
                $url = filter_var($url, FILTER_SANITIZE_URL);
                $url = explode('/', $url);
                return $url;
            }
        }

        public function initController(&$url) {

            if (file_exists('../app/controller/' . ucwords($url[0]).'.php')) {

                $this->currentController = ucwords($url[0]);
                unset($url[0]);
            }
            require_once '../app/controller/' . ucwords($this->currentController) . '.php';
            $this->currentController = new $this->currentController;
        }

        public function verifyMethod(&$url) {

            if (isset($url[1])) {

                if (method_exists($this->currentController, $url[1])) {

                    $this->currentMethod = $url[1];
                    unset($url[1]);
                }
            }
        }

    }

?>
