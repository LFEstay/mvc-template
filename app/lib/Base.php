<?php

    /**
     *
     */
    class Base {

        private $host = DB_HOST;
        private $user = DB_USER;
        private $psw = DB_PSW;
        private $dbName = DB_NAME;

        private $dbHandler;
        private $stmt;

        function __construct() {

            $dsn = 'mysql:host=' . $this->host . '; dbname=' . $this->dbName;
            $options = array(
                PDO::ATTR_PERSISTENT => true,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            );

            $this->connectDB($dsn, $options);
        }

        private function connectDB($dsn, $opt) {

            try {
                $this->dbHandler = new PDO($dsn, $this->user, $this->psw, $opt);

                $this->dbHandler->exec('set names utf8'); // for special characters

            } catch (PDOException $e) {
                echo $e->getMessage();
            }
        }

        public function prepareQuery($sql) {

            $this->stmt = $this->dbHandler->prepare();
        }

        public function bind($parameter, $value, $type = null) {

            // campo, valor, tipo
            if (is_null($type)) {

                switch (true) {
                    case is_int($value):
                        $type = PDO::PARAM_INT;
                    break;
                    case is_bool($value):
                        $type = PDO::PARAM_BOOL;
                    break;
                    case is_null($value):
                        $type = PDO::PARAM_NULL;
                    break;
                    default:
                        $type = PDO::PARAM_STR;
                        break;
                }
            }
            $this->stmt->bindValue($parameter, $value, $type);
        }

        public function executeQuery() {

            $query = $this->stmt->execute();
            return $query;
        }

        public function getAllRecords() {

            $this->executeQuery();
            $records = $this->stmt->fetchAll(PDO::FETCH_OBJ);
            return $records;
        }

        public function getRecord() {

            $this->executeQuery();
            $record = $this->stmt->fetch(PDO::FETCH_OBJ);
            return $record;
        }

        public function getRowsNumber() {

            $rows = $this->stmt->rowCount();
            return $rows;
        }

    }
?>
