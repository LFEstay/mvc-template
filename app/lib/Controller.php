<?php

    /* main controller. load model and view */

class Controller {

    function __construct() {
        //
    }

    public function accessModel($modelName) {


        require_once '../app/model/' . $modelName . '.php';
        $model = new $modelName();
        return $model;

    }

    public function loadView($viewName, $data = []) {

        if (file_exists('../app/view/'. $viewName . '.php')) {
            require_once '../app/view/'. $viewName . '.php';
        } else {
            die('view ' .$viewName . ' file not found');
        }

    }

}


?>
