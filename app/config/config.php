<?php

    define('DB_HOST', 'localhost');
    define('DB_USER', '_USER');
    define('DB_PSW', '_PASSWORD');
    define('DB_NAME', '_DATABASE');

    define('APP_PATH', dirname(dirname(__FILE__)));
    define('URL', '_URL');
    define('DOMAIN', '_DOMAIN');

?>
